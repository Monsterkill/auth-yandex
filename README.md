# Auth Yandex

![License](https://img.shields.io/badge/license-MIT-blue.svg) [![Latest Stable Version](https://img.shields.io/packagist/v/demlen/auth-yandex.svg)](https://packagist.org/packages/demlen/auth-yandex)

A [Flarum](http://flarum.org) extension. Auth Yandex

### Installation

```sh
composer require demlen/auth-yandex
```

### Updating

```sh
composer update demlen/auth-yandex
php flarum cache:clear
```

### Links

- [Packagist](https://packagist.org/packages/demlen/auth-yandex)
"# Auth-Yandex" 
