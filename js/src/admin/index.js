import app from 'flarum/app';

import YandexSettingsModal from './components/YandexSettingsModal';

app.initializers.add('demlen-auth-yandex', () => {
  app.extensionSettings['demlen-auth-yandex'] = () => app.modal.show(new YandexSettingsModal());
});
