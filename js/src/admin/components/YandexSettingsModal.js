import SettingsModal from 'flarum/components/SettingsModal';

export default class YandexSettingsModal extends SettingsModal {
  className() {
    return 'YandexSettingsModal Modal--small';
  }

  title() {
    return app.translator.trans('demlen-auth-yandex.admin.yandex_settings.title');
  }

  form() {
    return [
      <div className="Form-group">
        <label>{app.translator.trans('demlen-auth-yandex.admin.yandex_settings.app_id_label')}</label>
        <input className="FormControl" bidi={this.setting('demlen-auth-yandex.app_id')}/>
      </div>,

      <div className="Form-group">
        <label>{app.translator.trans('demlen-auth-yandex.admin.yandex_settings.app_password_label')}</label>
        <input className="FormControl" bidi={this.setting('demlen-auth-yandex.app_password')}/>
      </div>
    ];
  }
}
